<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.05.2019
 * Time: 11:09
 */

namespace app\controllers;


use app\models\MainModel;
use vendor\core\base\Controller;

class AppController extends Controller{

    public $menu;
    public $meta = [];
    public function __construct($route) {
        parent::__construct($route);
        new MainModel();
        $this->menu = \R::findAll('category');
        self::setMeta();
    }

    protected function setMeta($title = '', $desc = '', $keywords = ''){
        $this->meta['title'] =  $title;
        $this->meta['desc'] =  $desc;
        $this->meta['keywords'] =  $keywords;


    }



}