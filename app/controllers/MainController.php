<?php

namespace  app\controllers;

use app\models\MainModel;

class MainController extends AppController {

    //public $layout = 'main';
    public function indexAction(){

        $model = new MainModel();
//        $res = $model->query("CREATE TABLE IF NOT EXISTS `posts` (
//            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
//            `categoty_id` INT(10) UNSIGNED NOT NULL,
//            `title` VARCHAR(255) NOT NULL,
//            `excerpt` VARCHAR(255) NOT NULL,
//            `text` TEXT NOT NULL,
//            `keywords` VARCHAR(255) NULL,
//            `description` VARCHAR(255) NULL,
//             PRIMARY KEY (`id`))
//");

        $posts = \R::findAll('posts');
        $menu = $this->menu;
        $title = 'Main Title';
        $this->setMeta('Главная страница', 'Описание', 'Ключи');
        $meta = $this->meta;
        $this->set(compact('title', 'posts' , 'menu', 'meta'));

    }
}