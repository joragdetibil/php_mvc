<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.05.2019
 * Time: 4:06
 */

namespace app\controllers;


class PageController extends AppController {

    public function viewAction()
    {
        $menu = $this->menu;
        $title = 'Page Title';
        $meta = $this->meta;
        $this->set(compact('title', 'menu', 'meta'));
    }

}