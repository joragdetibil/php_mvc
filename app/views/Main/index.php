

<div class="container">
    <h1>Main index</h1>

    <?php if(!empty($posts)): ?>
        <?php foreach ($posts as $post): ?>
            <div class="panel panel-default">
                <div class="panel-heading"><?=$post['title']?></div>
                <div class="panel-body">
                    <?=$post['text']?>
                </div>
            </div>
        <?php endforeach;?>
    <?php endif; ?>
</div>
