<?php

require '..\vendor\libs\rb-mysql.php';
require '..\vendor\libs\functions.php';
$db = require '..\config\config_db.php';
R::setup($db['dsn'], $db['user'], $db['pass']);

//var_dump(R::testConnection());

$cat = R::dispense('category');
$cat->title = 'Категория 2';
$id = R::store($cat);
