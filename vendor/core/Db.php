<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.05.2019
 * Time: 13:10
 */

namespace vendor\core;

use R;

class Db{

    protected $pdo;
    protected static $instance;
    public static $countSql = 0;
    public static $queries = [];

    protected function __construct(){

        $db = require ROOT . '/config/config_db.php';
        require '..\vendor\libs\rb-mysql.php';
        R::setup($db['dsn'], $db['user'], $db['pass']);
        R::freeze(true);
    }

    public static function instance(){

        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

//    public function execute($sql,  $params = []){
//        self::$countSql++;
//        self::$queries[] = $sql;
//        $stmt = $this->pdo->prepare($sql);
//        return $stmt->execute($params);
//    }
//
//    public function query($sql, $params = []){
//        self::$countSql++;
//        self::$queries[] = $sql;
//        $stmt = $this->pdo->prepare($sql);
//        $res = $stmt->execute($params);
//        if($res !== false){
//            return $stmt->fetchAll();
//        }
//        return [];
//
//    }

}