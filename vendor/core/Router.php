<?php

namespace vendor\core;


class Router{

    protected static $routes = [];  /*таблица маршрутов*/
    protected static $route = [];   /*текущий адрес*/

    public static function add($regexp, $route = []){
        self::$routes[$regexp] = $route;
    }

    public static function getRoutes(){
        return self::$routes;
    }

    public static function getRoute(){
        return self::$route;
    }

    /**
     * ищет адрес в таблице маршрутов и преобразует в массив контроллер => метод
     */
    public static function matchRoute($url){
        foreach (self::$routes as $pattern => $route){
            if(preg_match("#$pattern#i", $url, $matches)){
                foreach ($matches as $key => $val){
                    if(is_string($key)){
                        $route[$key] = $val;
                    }
                }
                if(!isset($route['action'])){
                    $route['action'] = 'index';
                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                //debug($route);
                return true;
            }
        }
        return false;
    }

    /**
     * сопоставляет адрес url контроллеру и методу
     */
    public static function dispatch($url){
        $url = self::remoteQueryString($url);
        if(self::matchRoute($url)){
            $controller = 'app\controllers\\'. self::upperCamelCase(self::$route['controller'] . 'Controller');
            if(class_exists($controller)){
                $obj = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                if(method_exists($obj, $action)){
                    $obj->$action();
                    $obj->getView();
                }else{
                    echo "Method $controller::$action is not found";
                }
            }else{
                echo "Controller $controller is not found";
            }
        }else{
            http_response_code(404);
            include '404.html';
        }
    }

    protected static function upperCamelCase($name){
        $name = str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
        return $name;
    }

    protected static function lowerCamelCase($name){
        $name = lcfirst(self::upperCamelCase($name));
        return $name;
    }

    protected static function remoteQueryString($url){
        if($url){
            $params = explode('&', $url, 2);
            if(false === strpos($params[0], '=')){
                return rtrim($params[0], '/');
            }else{
                return '';
            }

        }
        return $url;
    }


}