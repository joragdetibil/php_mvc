<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.05.2019
 * Time: 3:54
 */

namespace vendor\core\base;


abstract class Controller{

    /**
     * @var array
     * текущий маршрут
     */
    public $route = [];

    /**
     * @var string
     * текущий вид
     */
    public $view;

    /**
     * @var string
     * текущий шаблон
     */
    public $layout;

    /**
     * @var array
     * пользовательские данные
     */
    public $vars = [];


    public function __construct($route){
        $this->route = $route;
        $this->view = $route['action'];

    }
    public function getView(){
        $viewObj = new View($this->route, $this->layout, $this->view);
        $viewObj->render($this->vars);
    }

    public function set($vars){

        $this->vars = $vars;
    }
}